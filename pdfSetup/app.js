var express = require('express');
var app     = express();
var fs      = require('fs');
var pdf     = require('html-pdf');

app.get('/', function (req, res) {

    var html    = fs.readFileSync('./index.html', 'utf8');
    var options = { 
        format      : 'A4',
        phantomPath : "./node_modules/phantomjs-prebuilt/lib/phantom/bin/phantomjs",
        base        : "file:///var/www/html/securedcardhtml/pdfSetup/",
        header      : {
            height      : "15mm",
            contents    : '<table width="100%" align="center" style="padding: 12px 32px;"><tr><td align="left"><table width="100%"><tr class="border_bottom"><td><img alt="logo" src="file:///var/www/html/securedcardhtml/pdfSetup/images/Paisabazaar-Logo.svg" /></td><td align="right"><img style="text-align:right;" alt="sbm_bank" src="file:///var/www/html/securedcardhtml/pdfSetup/images/SBM Bank - Logo.svg" /></td></tr></table></td></tr></table>'
        },
        footer      : {
            height      : "15mm",
            contents    : ''
        }
    };

    pdf.create(html, options).toBuffer(function(err, buffer) {
        if (err) return console.log(err);
        res.contentType("application/pdf");
        res.send(buffer);
    });
});

var server = app.listen(5000, function () {
   var host = server.address().address
   var port = server.address().port
   
   console.log("Example app listening at http://%s:%s", host, port)
})